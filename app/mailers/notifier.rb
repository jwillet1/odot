class Notifier < ActionMailer::Base
  default from: "noreply@example.com"
  
  def password_reset(user)
    @user = user
    mail(to:user.email, subject: "Reset Your Password")
  end
  
  def todo_list(todo_list, destination)
    @user = todo_list.user
    @todo_list = todo_list
    mail(to: destination, subject: "#{@user.first_name} sent you a todo list")
  end
end
